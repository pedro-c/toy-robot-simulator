package io.olympe;

import io.olympe.executor.ToyRobotExecutor;
import io.olympe.input.CommandTransformer;
import io.olympe.input.FileInputCommand;
import io.olympe.model.Command;
import io.olympe.model.Table;
import io.olympe.model.ToyRobot;
import io.olympe.output.StandardOutput;

import java.io.IOException;
import java.util.List;

public class ToyRobotSimulatorApp {

    public static void main(String[] args) throws IOException {
        StandardOutput standardOutput = new StandardOutput();
        if (args.length != 1){
            standardOutput.print("please add the path of the file");
        } else {
            FileInputCommand fileInputCommand = new FileInputCommand();
            List<String> inputs = fileInputCommand.readInputFromFile(args[0]);
            CommandTransformer commandTransformer = new CommandTransformer();
            List<Command> commands = commandTransformer.transform(inputs);
            ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(-1,-1, null), new Table(5,5), standardOutput);
            toyRobotExecutor.executeCommands(commands);
        }

    }
}
