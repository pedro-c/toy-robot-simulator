package io.olympe.exception;

public class InvalidCommandException extends RuntimeException {

    public InvalidCommandException(String errorMessage) {
        super(errorMessage);
    }

    public InvalidCommandException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

}
