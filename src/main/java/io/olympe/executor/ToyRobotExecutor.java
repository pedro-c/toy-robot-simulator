package io.olympe.executor;

import io.olympe.model.Command;
import io.olympe.model.Direction;
import io.olympe.model.Table;
import io.olympe.model.ToyRobot;
import io.olympe.output.StandardOutput;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ToyRobotExecutor {

    private ToyRobot toyRobot;
    private Table table;
    private StandardOutput standardOutput;

    public void executeCommands(List<Command> commands){
        commands.forEach(command -> execute(command));
    }

    private void execute(Command command) {
        switch (command.getCommandAction()){
            case PLACE:
                setToyRobotPosition(command);
                break;
            case LEFT:
                if (isToyRobotInsideTable(toyRobot.getPositionX(), toyRobot.getPositionY())){
                    turnRobotLeft();
                }
                break;
            case RIGHT:
                if (isToyRobotInsideTable(toyRobot.getPositionX(), toyRobot.getPositionY())){
                    turnRobotRight();
                }
                break;
            case REPORT:
                if (isToyRobotInsideTable(toyRobot.getPositionX(), toyRobot.getPositionY())){
                    reportToyRobot();
                }
                break;
            case MOVE:
                if (isToyRobotInsideTable(toyRobot.getPositionX(), toyRobot.getPositionY())){
                    moveToyRobot();
                }
                break;
        }
    }

    private void setToyRobotPosition(Command command) {
        toyRobot.setPositionX(command.getPositionX());
        toyRobot.setPositionY(command.getPositionY());
        toyRobot.setDirection(command.getDirection());
    }

    private void moveToyRobot() {
        int positionX = toyRobot.getPositionX();
        int positionY = toyRobot.getPositionY();

        switch (toyRobot.getDirection()){
            case EAST:
                positionX++;
                break;
            case NORTH:
                positionY++;
                break;
            case WEST:
                positionX--;
                break;
            case SOUTH:
                positionY--;
                break;
        };
        if (isToyRobotInsideTable(positionX, positionY)){
            toyRobot.setPositionX(positionX);
            toyRobot.setPositionY(positionY);
        }
    }



    private void reportToyRobot() {
        standardOutput.print(String.format("%d,%d,%s", toyRobot.getPositionX(), toyRobot.getPositionY(), toyRobot.getDirection()));
    }

    private void turnRobotLeft() {
        switch (toyRobot.getDirection()){
            case EAST:
                toyRobot.setDirection(Direction.NORTH);
                break;
            case NORTH:
                toyRobot.setDirection(Direction.WEST);
                break;
            case WEST:
                toyRobot.setDirection(Direction.SOUTH);
                break;
            case SOUTH:
                toyRobot.setDirection(Direction.EAST);
                break;
        };
    }

    private void turnRobotRight() {
        switch (toyRobot.getDirection()){
            case EAST:
                toyRobot.setDirection(Direction.SOUTH);
                break;
            case SOUTH:
                toyRobot.setDirection(Direction.WEST);
                break;
            case WEST:
                toyRobot.setDirection(Direction.NORTH);
                break;
            case NORTH:
                toyRobot.setDirection(Direction.EAST);
                break;
        };
    }

    private boolean isToyRobotInsideTable(Integer positionX, Integer positionY) {
        return  positionX >= 0 &&
                positionX < table.getLength() &&
                positionY >= 0 &&
                positionY < table.getWidth();
    }

}
