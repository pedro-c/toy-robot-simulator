package io.olympe.model;

public enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST;
}
