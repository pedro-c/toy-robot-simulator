package io.olympe.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Table {
    private final int length;
    private final int width;
}
