package io.olympe.model;

public enum CommandAction {
    PLACE,
    MOVE,
    LEFT,
    RIGHT,
    REPORT
}
