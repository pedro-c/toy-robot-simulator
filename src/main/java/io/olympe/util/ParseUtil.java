package io.olympe.util;

public class ParseUtil {

    private static final String COMMAND_SEPARATOR = "[\\s,]+";

    public static String[] getCommandInput(String rawDimensions){
        return rawDimensions.split(COMMAND_SEPARATOR);
    }

}
