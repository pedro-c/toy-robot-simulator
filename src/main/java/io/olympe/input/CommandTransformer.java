package io.olympe.input;

import io.olympe.exception.InvalidCommandException;
import io.olympe.model.Command;
import io.olympe.model.CommandAction;
import io.olympe.model.Direction;
import io.olympe.util.ParseUtil;

import java.util.List;
import java.util.stream.Collectors;

public class CommandTransformer {

    public static final int COMMAND_ACTION = 0;
    public static final int POSITION_X = 1;
    public static final int POSITION_Y = 2;
    public static final int DIRECTION = 3;

    public List<Command> transform(List<String> input){
        return input.stream()
                .map(i -> ParseUtil.getCommandInput(i))
                .map(j -> transformInput(j))
                .collect(Collectors.toList());
    }

    private Command transformInput(String[] input) {
        switch (input[COMMAND_ACTION]){
            case "PLACE":
                if (input.length != 4){
                    throw new InvalidCommandException("Invalid PLACE command. The command PLACE must be in the following format PLACE X Y F");
                }
                int positionX = getPosition(input[POSITION_X]);
                int positionY = getPosition(input[POSITION_Y]);;
                Direction direction = getDirection(input[DIRECTION]);
               return Command.builder().commandAction(CommandAction.PLACE).positionX(positionX).positionY(positionY).direction(direction).build();
            case "MOVE":
            case "LEFT":
            case "RIGHT":
            case "REPORT":
                return getCommand(input);
            default:
                throw new InvalidCommandException("Invalid command action. Must be one of the following: PLACE, MOVE, LEFT, RIGHT, REPORT");

        }
    }

    private Command getCommand(String[] input) {
        if (input.length != 1){
            throw new InvalidCommandException("Invalid " + input[0] + " command. The command " + input[0] + " must only have the command action.");
        }
        return Command.builder().commandAction(CommandAction.valueOf(input[0])).build();
    }

    private Direction getDirection(String direction) {
        switch (direction){
            case "NORTH":
                return Direction.NORTH;
            case "SOUTH":
                return Direction.SOUTH;
            case "EAST":
                return Direction.EAST;
            case "WEST":
                return Direction.WEST;
            default:
                throw new InvalidCommandException("Invalid command direction. Must be one of the following: NORTH, SOUTH, EAST, WEST");
        }
    }

    private int getPosition(String position) {
        try{
            return Integer.parseInt(position);
        } catch (NumberFormatException e){
            throw new InvalidCommandException("Invalid PLACE command. position should be a integer", e);
        }
    }
}
