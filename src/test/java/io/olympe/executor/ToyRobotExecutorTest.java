package io.olympe.executor;

import io.olympe.model.*;
import io.olympe.output.StandardOutput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ToyRobotExecutorTest {

    @Mock
    StandardOutput standardOutput;

    @Test
    void executeCommands_placeCommandInsideTable_ToyRobotUpdated() {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(1)
                .positionY(2)
                .direction(Direction.EAST)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), new StandardOutput());
        toyRobotExecutor.executeCommands(commands);

        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(place.getDirection(),toyRobotExecutor.getToyRobot().getDirection());
    }

    @Test
    void executeCommands_placeCommandOutsideTable_ToyRobotUpdated() {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(-1)
                .positionY(-2)
                .direction(Direction.EAST)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), new StandardOutput());
        toyRobotExecutor.executeCommands(commands);

        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(place.getDirection(),toyRobotExecutor.getToyRobot().getDirection());
    }

    @ParameterizedTest
    @EnumSource(value = CommandAction.class, names = {"MOVE", "LEFT", "RIGHT"})
    void executeCommands_placeCommandOutsideTableAndAnotherCommand_ToyRobotNotUpdatedAfterPLaceCommand(CommandAction commandAction) {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(-1)
                .positionY(-2)
                .direction(Direction.EAST)
                .build();
        Command command = Command.builder()
                .commandAction(commandAction)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        commands.add(command);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), new StandardOutput());
        toyRobotExecutor.executeCommands(commands);

        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(place.getDirection(),toyRobotExecutor.getToyRobot().getDirection());
    }

    @Test
    void executeCommands_placeCommandOutsideTableAndReportCommand_noCommandExecution() {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(-1)
                .positionY(-2)
                .direction(Direction.EAST)
                .build();
        Command report = Command.builder()
                .commandAction(CommandAction.REPORT)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        commands.add(report);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), standardOutput);

        toyRobotExecutor.executeCommands(commands);

        verify(standardOutput, times(0)).print(anyString());
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(place.getDirection(),toyRobotExecutor.getToyRobot().getDirection());
    }

    @Test
    void executeCommands_placeCommandInsideTableAndReportCommand_reportExecution() {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(0)
                .positionY(0)
                .direction(Direction.EAST)
                .build();
        Command report = Command.builder()
                .commandAction(CommandAction.REPORT)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        commands.add(report);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), standardOutput);
        doNothing().when(standardOutput).print(anyString());

        toyRobotExecutor.executeCommands(commands);

        verify(standardOutput, times(1)).print(anyString());
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(place.getDirection(),toyRobotExecutor.getToyRobot().getDirection());
    }

    @Test
    void executeCommands_placeCommandInsideTableAndAnoLeftCommand_ToyRobotUpdateDirection() {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(1)
                .positionY(1)
                .direction(Direction.NORTH)
                .build();
        Command left = Command.builder()
                .commandAction(CommandAction.LEFT)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        commands.add(left);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), new StandardOutput());
        toyRobotExecutor.executeCommands(commands);

        // Test change direction NORTH -> WEST
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(Direction.WEST,toyRobotExecutor.getToyRobot().getDirection());

        // Test change direction WEST -> SOUTH
        commands.clear();
        commands.add(left);
        toyRobotExecutor.executeCommands(commands);
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(Direction.SOUTH,toyRobotExecutor.getToyRobot().getDirection());

        commands.clear();
        commands.add(left);
        toyRobotExecutor.executeCommands(commands);
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(Direction.EAST,toyRobotExecutor.getToyRobot().getDirection());

        commands.clear();
        commands.add(left);
        toyRobotExecutor.executeCommands(commands);
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(Direction.NORTH,toyRobotExecutor.getToyRobot().getDirection());
    }

    @Test
    void executeCommands_placeCommandInsideTableAndAnoRightCommand_ToyRobotUpdateDirection() {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(1)
                .positionY(1)
                .direction(Direction.NORTH)
                .build();
        Command right = Command.builder()
                .commandAction(CommandAction.RIGHT)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        commands.add(right);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), new StandardOutput());
        toyRobotExecutor.executeCommands(commands);

        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(Direction.EAST,toyRobotExecutor.getToyRobot().getDirection());

        commands.clear();
        commands.add(right);
        toyRobotExecutor.executeCommands(commands);
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(Direction.SOUTH,toyRobotExecutor.getToyRobot().getDirection());

        commands.clear();
        commands.add(right);
        toyRobotExecutor.executeCommands(commands);
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(Direction.WEST,toyRobotExecutor.getToyRobot().getDirection());

        commands.clear();
        commands.add(right);
        toyRobotExecutor.executeCommands(commands);
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(Direction.NORTH,toyRobotExecutor.getToyRobot().getDirection());
    }

    @Test
    void executeCommands_placeCommandInsideTableAndAnoMoveCommandToOutsideSouthDirection_ToyRobotNotUpdated() {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(0)
                .positionY(0)
                .direction(Direction.SOUTH)
                .build();
        Command move = Command.builder()
                .commandAction(CommandAction.MOVE)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        commands.add(move);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), new StandardOutput());
        toyRobotExecutor.executeCommands(commands);
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(place.getDirection(),toyRobotExecutor.getToyRobot().getDirection());
    }

    @Test
    void executeCommands_placeCommandInsideTableAndAnoMoveCommandToOutsideWestDirection_ToyRobotNotUpdated() {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(0)
                .positionY(0)
                .direction(Direction.WEST)
                .build();
        Command move = Command.builder()
                .commandAction(CommandAction.MOVE)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        commands.add(move);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), new StandardOutput());
        toyRobotExecutor.executeCommands(commands);
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(place.getDirection(),toyRobotExecutor.getToyRobot().getDirection());
    }

    @Test
    void executeCommands_placeCommandInsideTableAndAnoMoveCommandToOutsideNorthDirection_ToyRobotNotUpdated() {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(5)
                .positionY(5)
                .direction(Direction.NORTH)
                .build();
        Command move = Command.builder()
                .commandAction(CommandAction.MOVE)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        commands.add(move);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), new StandardOutput());
        toyRobotExecutor.executeCommands(commands);
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(place.getDirection(),toyRobotExecutor.getToyRobot().getDirection());
    }

    @Test
    void executeCommands_placeCommandInsideTableAndAnoMoveCommandToOutsideEastDirection_ToyRobotNotUpdated() {
        Command place = Command.builder()
                .commandAction(CommandAction.PLACE)
                .positionX(5)
                .positionY(5)
                .direction(Direction.EAST)
                .build();
        Command move = Command.builder()
                .commandAction(CommandAction.MOVE)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(place);
        commands.add(move);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(), new Table(5,5), new StandardOutput());
        toyRobotExecutor.executeCommands(commands);
        assertEquals(place.getPositionX(),toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(place.getPositionY(),toyRobotExecutor.getToyRobot().getPositionY());
        assertEquals(place.getDirection(),toyRobotExecutor.getToyRobot().getDirection());
    }

    @ParameterizedTest
    @EnumSource(value = CommandAction.class, names = {"MOVE", "LEFT", "RIGHT"})
    void executeCommands_otherCommandWithoutPreviousPlaceCommand_ToyRobotNotUpdated(CommandAction commandAction) {
        Command command = Command.builder()
                .commandAction(commandAction)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(command);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(-1,-1,null), new Table(5,5), new StandardOutput());
        toyRobotExecutor.executeCommands(commands);

        assertEquals(-1, toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(-1, toyRobotExecutor.getToyRobot().getPositionY());
        assertNull(toyRobotExecutor.getToyRobot().getDirection());
    }

    @Test
    void executeCommands_reportCommandWithoutPreviousPlaceCommand_reportNotShown() {
        Command report = Command.builder()
                .commandAction(CommandAction.REPORT)
                .build();
        List<Command> commands = new ArrayList<>();
        commands.add(report);
        ToyRobotExecutor toyRobotExecutor = new ToyRobotExecutor(new ToyRobot(-1,-1, null), new Table(5,5), standardOutput);
        toyRobotExecutor.executeCommands(commands);

        verify(standardOutput, times(0)).print(anyString());
        assertEquals(-1, toyRobotExecutor.getToyRobot().getPositionX());
        assertEquals(-1, toyRobotExecutor.getToyRobot().getPositionY());
        assertNull(toyRobotExecutor.getToyRobot().getDirection());
    }

}