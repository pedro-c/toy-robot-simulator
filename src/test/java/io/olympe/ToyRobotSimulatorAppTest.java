package io.olympe;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class ToyRobotSimulatorAppTest {

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    void main_validInput_01NORTH() throws IOException {
        String[] args = new String[]{"src/test/resources/validInput"};
        ToyRobotSimulatorApp.main(args);

        assertEquals("0,1,NORTH", outputStreamCaptor.toString().trim());
    }

    @Test
    void main_exampleA_00WEST() throws IOException {
        String[] args = new String[]{"src/test/resources/example_a"};
        ToyRobotSimulatorApp.main(args);

        assertEquals("0,0,WEST", outputStreamCaptor.toString().trim());
    }

    @Test
    void main_exampleB_33NORTH() throws IOException {
        String[] args = new String[]{"src/test/resources/example_b"};
        ToyRobotSimulatorApp.main(args);

        assertEquals("3,3,NORTH", outputStreamCaptor.toString().trim());
    }

    @Test
    void main_exampleC_20EAST() throws IOException {
        String[] args = new String[]{"src/test/resources/example_c"};
        ToyRobotSimulatorApp.main(args);

        assertEquals("2,0,EAST", outputStreamCaptor.toString().trim());
    }

    @Test
    void main_noArgs_warningMessage() throws IOException {
        String[] args = new String[]{};
        ToyRobotSimulatorApp.main(args);

        assertEquals("please add the path of the file", outputStreamCaptor.toString().trim());
    }

    @Test
    void main_invalidFilePath_ioException() throws IOException {
        String[] args = new String[]{"noFile"};

        Exception exception = assertThrows(IOException.class, () -> {
            ToyRobotSimulatorApp.main(args);
        });

        String expectedMessage = "Error while trying to read the file";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

}