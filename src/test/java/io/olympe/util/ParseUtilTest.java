package io.olympe.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class ParseUtilTest {

    @Test
    void getCommandInput_placeCommand_arrayOfAllCommandProperties() {
        String[] commandInput = ParseUtil.getCommandInput("PLACE 0,0,NORTH");

        String[] expectedCommandInput = new String[]{"PLACE", "0", "0", "NORTH"};
        assertArrayEquals( expectedCommandInput, commandInput);
    }

    @Test
    void getCommandInput_reportCommand_arrayOfAllCommandProperties() {
        String[] commandInput = ParseUtil.getCommandInput("REPORT");

        String[] expectedCommandInput = new String[]{"REPORT"};
        assertArrayEquals( expectedCommandInput, commandInput);
    }
}