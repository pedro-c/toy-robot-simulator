package io.olympe.input;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileInputCommandTest {

    @Test
    void readInputFromFile_invalidFilePath_IOException() {
        FileInputCommand fileInputCommand = new FileInputCommand();

        Exception exception = assertThrows(IOException.class, () -> {
            fileInputCommand.readInputFromFile("noFile");
        });

        String expectedMessage = "Error while trying to read the file";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void readInputFromFile_validFilePath_IOExce() throws IOException {
        FileInputCommand fileInputCommand = new FileInputCommand();
        List<String> inputs = fileInputCommand.readInputFromFile("src/test/resources/validInput");

        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add("PLACE 0,0,NORTH");
        expectedInputs.add("MOVE");
        expectedInputs.add("REPORT");

        assertEquals(expectedInputs, inputs);
    }
}