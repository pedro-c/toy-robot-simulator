package io.olympe.input;

import io.olympe.exception.InvalidCommandException;
import io.olympe.model.Command;
import io.olympe.model.CommandAction;
import io.olympe.model.Direction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static io.olympe.model.CommandAction.PLACE;
import static io.olympe.model.Direction.NORTH;
import static org.junit.jupiter.api.Assertions.*;

class CommandTransformerTest {

    @ParameterizedTest
    @EnumSource(Direction.class)
    void transform_placeWithDirection_validPlaceCommand(Direction direction) {
        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add("PLACE 1,2," + direction.toString());
        CommandTransformer commandTransformer = new CommandTransformer();
        List<Command> commands = commandTransformer.transform(expectedInputs);

        assertEquals(1, commands.size());
        assertEquals(PLACE, commands.get(0).getCommandAction());
        assertEquals(1, commands.get(0).getPositionX());
        assertEquals(2, commands.get(0).getPositionY());
        assertEquals(direction, commands.get(0).getDirection());
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, -3, 15, Integer.MAX_VALUE, Integer.MIN_VALUE})
    void transform_placeWithDifferentRangeValues_validPlaceCommand(int position) {
        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add("PLACE " + position + "," + position + ",NORTH");
        CommandTransformer commandTransformer = new CommandTransformer();
        List<Command> commands = commandTransformer.transform(expectedInputs);

        assertEquals(1, commands.size());
        assertEquals(PLACE, commands.get(0).getCommandAction());
        assertEquals(position, commands.get(0).getPositionX());
        assertEquals(position, commands.get(0).getPositionY());
        assertEquals(NORTH, commands.get(0).getDirection());
    }

    @ParameterizedTest
    @EnumSource(value = CommandAction.class, names = {"MOVE", "LEFT", "RIGHT", "REPORT"})
    void transform_otherCommandActions_validCommand(CommandAction commandAction) {
        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add(commandAction.toString());
        CommandTransformer commandTransformer = new CommandTransformer();
        List<Command> commands = commandTransformer.transform(expectedInputs);

        assertEquals(1, commands.size());
        assertEquals(commandAction, commands.get(0).getCommandAction());
        assertNull(commands.get(0).getPositionX());
        assertNull(commands.get(0).getPositionY());
        assertNull(commands.get(0).getDirection());
    }

    @ParameterizedTest
    @EnumSource(value = CommandAction.class, names = {"MOVE", "LEFT", "RIGHT", "REPORT"})
    void transform_invalidNoPlaceCommand_InvalidCommandException(CommandAction commandAction) {
        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add(commandAction.toString() + " 1,1,NORTH");
        CommandTransformer commandTransformer = new CommandTransformer();

        Exception exception = assertThrows(InvalidCommandException.class, () -> {
            commandTransformer.transform(expectedInputs);
        });

        String expectedMessage = "Invalid " + commandAction.toString() + " command. The command " + commandAction.toString() + " must only have the command action.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void transform_invalidPlaceCommandLessProperties_InvalidCommandException() {
        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add("PLACE 1,2");
        CommandTransformer commandTransformer = new CommandTransformer();

        Exception exception = assertThrows(InvalidCommandException.class, () -> {
            commandTransformer.transform(expectedInputs);
        });

        String expectedMessage = "Invalid PLACE command. The command PLACE must be in the following format PLACE X Y F";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void transform_invalidPlaceCommandMoreProperties_InvalidCommandException() {
        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add("PLACE 1,2,NORTH,1");
        CommandTransformer commandTransformer = new CommandTransformer();

        Exception exception = assertThrows(InvalidCommandException.class, () -> {
            commandTransformer.transform(expectedInputs);
        });

        String expectedMessage = "Invalid PLACE command. The command PLACE must be in the following format PLACE X Y F";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void transform_invalidCommandPosition_InvalidCommandException() {
        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add("PLACE A,2,NORTH");
        CommandTransformer commandTransformer = new CommandTransformer();

        Exception exception = assertThrows(InvalidCommandException.class, () -> {
            commandTransformer.transform(expectedInputs);
        });

        String expectedMessage = "Invalid PLACE command. position should be a integer";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void transform_invalidCommandAction_InvalidCommandException() {
        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add("JUMP A,2,NORTH");
        CommandTransformer commandTransformer = new CommandTransformer();

        Exception exception = assertThrows(InvalidCommandException.class, () -> {
            commandTransformer.transform(expectedInputs);
        });

        String expectedMessage = "Invalid command action. Must be one of the following: PLACE, MOVE, LEFT, RIGHT, REPORT";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void transform_invalidDirection_InvalidCommandException() {
        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add("PLACE 1,2,SOUTH_WEST");
        CommandTransformer commandTransformer = new CommandTransformer();

        Exception exception = assertThrows(InvalidCommandException.class, () -> {
            commandTransformer.transform(expectedInputs);
        });

        String expectedMessage = "Invalid command direction. Must be one of the following: NORTH, SOUTH, EAST, WEST";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void transform_emptyCommand_InvalidCommandException() {
        List<String> expectedInputs = new ArrayList<>();
        expectedInputs.add("");
        CommandTransformer commandTransformer = new CommandTransformer();

        Exception exception = assertThrows(InvalidCommandException.class, () -> {
            commandTransformer.transform(expectedInputs);
        });

        String expectedMessage = "Invalid command action. Must be one of the following: PLACE, MOVE, LEFT, RIGHT, REPORT";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }




}